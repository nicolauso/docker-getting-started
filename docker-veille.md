# DOCKER

# Ressources
- https://www.ibm.com/fr-fr/topics/docker
- https://www.next-decision.fr/wiki/qu-est-ce-que-docker
- https://docs.docker.com/get-started/

# C'est quoi Docker ?
Docker est un logiciel Open Source, distribué à partir de mars 2013. C'est une plateforme de virtualisation par conteneur. C’est un outil qui utilise des conteneurs pour faciliter la création, le déploiement et l’exécution d’une application. Il permet de lier une application et ses dépendances à l’intérieur d’un conteneur.

## À quoi sert Docker ?
Les conteneurs simplifient le développement et la distribution d'applications distribuées.

## Quelles différences entres les containers Docker et les machines virtuelles ?
Une machine virtuelle (VM) est une émulation logicielle d'un ordinateur ou d'un système informatique, qui fonctionne comme une entité indépendante avec son propre système d'exploitation (OS) et ses ressources matérielles virtuelles. En d'autres termes, elle simule le comportement d'un ordinateur physique à l'intérieur d'un environnement logiciel.

Les containers Docker ne portent que certaines briques logicielles de l'OS grâce au coeur Linux ainsi que toutes les dépendances associées à l'application.

### Plus de légereté 
Contrairement aux machines virtuelles, les conteneurs ne portent pas le contenu d'une instance de système d'exploitation et d'un hyperviseur entiers. Ils ne comprennent que les processus et les dépendances du système d'exploitation nécessaires à l'exécution du code. Les conteneurs sont mesurés en mégaoctets (alors que les VM le sont en gigaoctets), ils utilisent plus efficacement la capacité matérielle et démarrent plus rapidement. 

## Meilleure productivité
- Ecrite une fois, déployable partout.
- Comparés aux machines virtuelles, les conteneurs sont plus rapides et plus faciles à déployer, à allouer et à redémarrer. Ils constituent donc une solution idéale pour les pipelines d'intégration continue et de distribution continue (CI/CD) et conviennent mieux aux équipes de développement qui adoptent les pratiques Agile et DevOps
  
## Optimisation des ressources
Les conteneurs permettent aux dév d'exécuter plusieurs fois plus de copies d'une application sur le même matériel qu'avec des VM, ce qui peut réduire les dépenses cloud.

# Avantages des containers
Comparé à LXC, Docker présente les avantages suivants :

## Portabilité améliorée et transparente 
Alors que les conteneurs LXC font souvent référence à des configurations spécifiques à une machine, les conteneurs Docker s'exécutent sans modification sur n'importe quel ordinateur de bureau, centre de données et environnement cloud. 

## Mises à jour toujours plus légères et granulaires
Avec LXC, plusieurs processus peuvent être combinés dans un seul conteneur. Ainsi, vous pouvez générer une application qui peut continuer à s'exécuter, alors qu'une de ses parties est arrêtée pour une mise à jour ou une réparation. 

## Création automatique des conteneurs
Docker peut générer automatiquement un conteneur basé sur le code source de l'application. 

## Gestion des versions
Docker peut assurer le suivi des versions d'une image de conteneur, revenir aux versions précédentes et déterminer qui a créé une version et comment elle a été créée. Il peut même télécharger uniquement les deltas entre une version existante et une nouvelle version. 

## Réutilisation des conteneurs 
Les conteneurs existants peuvent être utilisés comme images de base, essentiellement comme des modèles pour la création de conteneurs. 

## Bibliothèques de conteneurs partagés
Les développeurs peuvent accéder à un registre open source contenant des milliers de conteneurs auxquels contribuent les utilisateurs. 

# Comment marche Docker ?
Docker fonctionne par containeurisation des applications. Cette containeurisation est possible grâce aux fonctionnalités d'isolement et de virtualisation des processus intégrées au noyau Linux.


## DockerFiles
Chaque container comprend un /*DockerFiles*/. C'est le fichier qui porte les instructions nécessaires à la création d'image du container. DockerFile automatise le processus de création d'image Docker.
Il s'agit essentiellement d'une liste d'instructions d'interface de ligne de commande (CLI) que le moteur Docker exécute pour assembler l'image. La liste des commandes Docker est normalisée : les opérations Docker fonctionnent de la même manière, quels que soient le contenu, l'infrastructure ou les autres variables d'environnement. 

## Images
Les images Docker contiennent le code source exécutable de l'application, ainsi que tous les outils, bibliothèques et dépendances dont le code de l'application a besoin pour fonctionner en tant que conteneur. Lorsque vous exécutez une image Docker, elle devient une instance (ou plusieurs instances) du conteneur. 

## Containers
Les conteneurs Docker sont les instances actives en cours d'exécution des images Docker. Alors que les images Docker sont des fichiers en lecture seule, les conteneurs sont des contenus actifs, éphémères et exécutables. Les utilisateurs peuvent interagir avec eux et les administrateurs peuvent ajuster leurs paramètres et conditions à l'aide de commandes Docker. 

## Volumes
Le volumes sont un type de stockage Docker qui permet de persister les données d’un conteneur (par défaut aucune donnée n’est persistée). Ce qui signifie que s’il n’y a pas la présence de volume, un conteneur perd toutes ses données lorsqu’il est arrêté.

Dans Docker on retrouve 3 types de stockages :
    Volume
    Bind mounts
    Tmpfs mount
Une fois un volume créé, le client aura juste à démarrer le conteneur dans le volume et les données de ce conteneur seront persistées.

## Réseau Docker
Pour que les conteneurs puissent communiquer entre eux, Docker permet la création de réseau. Les réseaux permettent une meilleure isolation des conteneurs. Docker utilise plusieurs drivers pour connecter les conteneurs entre eux.
    Il y a 5 pilotes réseaux pour les conteneurs :
    Bridge (driver utilisé par défaut)
    Host
    Overlay
    Macvlan
    None

## Docker Desktop

## Déploiement
- Docker Compose
Utilisé pour gérer des applications multi-conteneurs, dans lesquelles tous les conteneurs s'exécutent sur le même hôte Docker. Docker Compose crée un fichier YAML (.YML) qui spécifie les services inclus dans l'application, et peut déployer et exécuter des conteneurs à l'aide d'une seule commande. Comme la syntaxe YAML est indépendante du langage, les fichiers YAML peuvent être utilisés dans des programmes écrits en Java, Python, Ruby et dans de nombreux autres langages. 

Les développeurs peuvent également utiliser Docker Compose pour définir des volumes persistants pour le stockage, spécifier des nœuds de base et documenter et configurer les dépendances de service.

- Kubernetes
La surveillance et la gestion des cycles de vie des conteneurs dans des environnements plus complexes nécessitent un outil d'orchestration de conteneur. Alors que Docker inclut son propre outil d'orchestration (appelé Docker Swarm), la plupart des développeurs choisissent plutôt Kubernetes. 

Kubernetes est une plateforme d'orchestration de conteneur open source, issue d'un projet développé en interne chez Google. Kubernetes planifie et automatise les tâches qui font partie intégrante de la gestion des architectures basées sur des conteneurs, notamment le déploiement des conteneurs, les mises à jour, la reconnaissance de services, l'allocation de l'espace de stockage, l'équilibrage de charge, le contrôle de l'intégrité, etc. En outre, l'écosystème d'outils open source pour Kubernetes, notamment Istio et Knative,permet aux entreprises de déployer une plateforme en tant que service (PaaS) à haute productivité pour les applications conteneurisées et de passer plus rapidement à l'informatique sans serveur.
- Plug-in Docker

### Les données persistent-elles lorsque qu'on stoppe et redémarre un conteneur ?
Non, sans système de persistence des données, les données du container "disparaissent" après l'arrêt de ce dernier.

### Qu'est-ce qu'un volume ?
Un volume est un système de stockage des données Docker. Les volumes Docker sont stockés dans un répertoire spécifique sur l'hôte Docker, généralement dans le dossier /var/lib/docker/volumes.
Les volumes peuvent être gérés à l'aide de commandes Docker dédiées telles que docker volume create, docker volume rm, docker volume ls, etc.
Exemple : docker run -v mon_volume:/chemin/dans/le/conteneur mon_image

### Qu'est-ce qu'un bind mount, et quelle est la différence avec un volume ?
Les bind mounts permettent de monter un répertoire ou un fichier de l'hôte dans le conteneur Docker.
Ils permettent de partager des données entre l'hôte et le conteneur Docker en montant un chemin d'accès sur l'hôte dans le conteneur.
Contrairement aux volumes, les bind mounts dépendent directement de la structure du système de fichiers de l'hôte, ce qui signifie qu'ils n'ont pas besoin d'être explicitement créés ou gérés par Docker.
Les bind mounts sont souvent utilisés pour fournir des fichiers de configuration ou des données spécifiques à l'environnement hôte dans le conteneur.
Exemple : docker run -v /chemin/sur/hote:/chemin/dans/conteneur mon_image

### Tmpfs
Tmpfs mounts (Montages de type "Tmpfs") :
Les tmpfs mounts permettent de monter un espace de stockage en mémoire RAM temporaire dans le conteneur. Ils sont utilisés pour stocker des données temporaires qui ne nécessitent pas de persistance sur le disque.
Les données stockées dans un tmpfs mount sont effacées lorsque le conteneur est arrêté ou supprimé.
Les tmpfs mounts sont souvent utilisés pour stocker des fichiers temporaires ou des données de cache nécessitant une performance élevée.
Exemple : docker run --tmpfs /chemin/dans/conteneur:rw,size=512m mon_image